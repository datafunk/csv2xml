# CSV to XML transformer

Nodejs CLI tool to convert CSV to XML.
~~Right now it's very specific to a particular purpose, but it shouldn't take too much to add a level of abstraction to generate nodes from column headers.~~
From v0.2.0 entities within rows are generated based on CSV column headers

## requirements and dependencies

Nothing tricky so far, look in package.json and use node v4.6.0 LTS (via NVM)

## Install and run

```
npm install
```

then

```
node index.js [path/to/input.csv] [path/to/output.xml]
```


# Google sheet driven...

Since v0.3.0 it accepts google sheet based data.
See https://docs.google.com/spreadsheets/d/1Z4fPRLGu2PruEOW4bW9JzMXJyq83o_q_5dPvuUxwidw/pub?output=csv

---

That's it for now, it does what it set out to do: creating a specific XML file to be feed into Navori TV VMS.
