#!/usr/bin/env node

'use strict'

const fs = require( 'fs' )
const http = require( 'http' )
const https = require( 'https' )
const mime = require( 'mime' )

const csv = process.argv[ 2 ] // input CSV file
const out = process.argv[ 3 ] // output filename
const pjson = require( './package.json' )

const Papa = require( 'papaparse' )
const papa_config = {
    delimiter: ",", // auto-detect
    newline: "", // auto-detect
    header: true,
    dynamicTyping: false,
    preview: 0,
    encoding: "utf-8",
    worker: false,
    comments: false,
    step: undefined,
    complete: undefined,
    error: undefined,
    download: false,
    skipEmptyLines: true,
    chunk: undefined,
    fastMode: false,
    beforeFirstChunk: undefined,
    withCredentials: undefined
}

var csv2xml = {}
exports = module.exports = csv2xml


csv2xml = (function(){
    // console.log( mime.lookup(csv) )
    if ( typeof ( csv ) !== 'string' || typeof ( out ) !== 'string' ) {
        console.log( 'CSV to XML parser ' + pjson.version )
        console.log(
            'Usage: node index.js [path/to/input/file.csv] [path/to/outFile.xml]'
        )
        return 1
    } else if( mime.lookup(csv) !== "text/csv") {
        console.log( 'CSV to XML parser ' + pjson.version )
        console.log(' The input file must have a MIME type of "text/csv"')
        return 1
    }


    if ( csv.match( 'http' ) || csv.match( 'https' ) ) {
        // console.log( 'remote file HTTP' )

        var result = ''

        function joinResults( res ) {
            result = result + res
            return result
        }

        let req = function ( data ) {
            data.setEncoding( 'utf-8' )

            data.on( 'error', function ( error ) {
                console.error( error )
            } )

            data.on( 'data', function ( res ) {
                // console.log(res)
                joinResults( res )
            } )

            data.on( 'end', function () {
                // console.log(result.length)
                // console.log(result)
                parseCSV( result )
            } )

        }

        https.get( csv, req )

    } else {
        // console.log('local file')
        fs.readFile( csv, 'utf8', function ( err, data ) {
            if ( err ) throw err
            console.log( data )
            parseCSV( data )
        } )
    }


    var xml = ''
    xml += '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + '\n'
    xml += '<Root xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n'


    function parseCSV( data ) {
        let parsed = Papa.parse( data, papa_config )
        // console.log(parsed.data[0])
        // console.log(parsed.meta)
        // console.log('=========')

        for ( let i = 0; i < parsed.data.length; i++ ) {
            console.log( parsed.data[ i ] )
            console.log( '--------------' )
            processRow( parsed.data[ i ], parsed.meta.fields )
        }

        callback()

    }

    function processRow( data_row, meta_fields ) {
        let row = ''
        row += '\t<Row>' + '\n'

        for ( let j = 0; j < meta_fields.length; j++ ) {

            meta_fields[ j ] = meta_fields[ j ].replace( /\s/g, '_' )

            row += '\t\t' + '<' + meta_fields[ j ] + '>\n'
            row += '\t\t\t' + data_row[ meta_fields[ j ] ] + '\n'
            row += '\t\t' + '</' + meta_fields[ j ] + '>' + '\n'
        }

        row += '\t</Row>' + '\n'
        xml += row
    }



    function callback() {
        xml += '</Root>'
        // console.log(xml)

        fs.writeFile( out, xml, 'utf-8', ( err ) => {
            if ( err ) throw err
            console.log( 'Saved to ' + out )
        } )
    }

})()
